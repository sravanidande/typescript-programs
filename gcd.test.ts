import { gcd } from './gcd';

test('gcd', () => {
  expect(gcd(3, 6)).toEqual(3);
  expect(gcd(1, 5)).toEqual(1);
  expect(gcd(8, 16)).toEqual(8);
});
