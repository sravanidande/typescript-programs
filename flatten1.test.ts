import {concat} from './mapcat';

import {flatten} from './flatten';

test('flatten', () => {
  expect(flatten([[2,3,4],[5,6],[7,8]])).toEqual([2,3,4,5,6,7,8]);
  expect(flatten([[2],[5],[7]])).toEqual([2,5,7]);  
  expect(flatten([[2,3,4,5],[7]])).toEqual([2,3,4,5,7]);  
});

