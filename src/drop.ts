export function drop(n: number, arr: number[]): number[] {
  const result: number[] = [];
  for (let e = n; e < arr.length; e++) {
    result.push(arr[e]);
  }
  return result;
}
