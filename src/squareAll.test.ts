import { squareAll } from './squareAll';

test('squareAll', () => {
  expect(squareAll([2, 3, 4])).toEqual([4, 9, 16]);
  expect(squareAll([0, 1])).toEqual([0, 1]);
  expect(squareAll([6,7])).toEqual([36,49]);
)};
