import { map } from './map';

test('map', () => {
  expect(map(x => x + 1, [1, 2, 3])).toEqual([2, 3, 4]);
  expect(map(x => x + 1, [10, 11])).toEqual([11, 12]);
});
