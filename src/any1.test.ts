import { any, even } from './any1';

test('any', () => {
  expect(any(even, [2, 3, 4, 5])).toEqual(true);
  expect(any(even, [1, 3, 6])).toEqual(true);
  expect(any(even, [2, 4, 5])).toEqual(true);
  expect(any(even, [1, 3, 5])).toEqual(true);
});
