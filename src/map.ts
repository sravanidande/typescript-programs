export function map(f: (x: number) => number, arr: number[]): number[] {
  const result = [];
  for (const e of arr) {
    result.push(f(e));
  }
  return result;
}
