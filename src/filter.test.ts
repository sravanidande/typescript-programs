import filter from './filter';

test('filter', () => {
  expect(filter(x => x % 2 === 0, [2, 3, 4, 5])).toEqual([2, 4]);
  expect(filter(x => x % 2 === 0, [1, 3, 6, 8, 4, 7])).toEqual([6, 8, 4]);
});
