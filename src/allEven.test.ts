import allEven from './allEven';

test('allEven', () => {
  expect(allEven([2, 3, 4, 5])).toEqual([2, 4]);
  expect(allEven([1, 3, 6, 8, 4, 7])).toEqual([6, 8, 4]);
)};
