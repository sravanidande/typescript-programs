import Stack from './stack';

test('stack', () => {
  const r = new Stack();
  expect(r.push(5)).toEqual(1);
  expect(r.push(10)).toEqual(2);
  expect(r.push(20)).toEqual(3);
  expect(r.pop()).toEqual(20);
  expect(r.pop()).toEqual(10);
  expect(r.isEmpty()).toBeFalsy();
  expect(r.size()).toEqual(1);
});
