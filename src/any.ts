export function even(x: number): boolean {
  if (x % 2 === 0) {
    return true;
  }
  return false;
}

export function any(f: (x: number) => boolean, arr: number[]): boolean {
  let result = false;
  for (const e = 0; e < arr.length; e++) {
    result = result || f(e);
    return result;
  }
}
