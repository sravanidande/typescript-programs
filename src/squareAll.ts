export function squareAll(arr: number[]): number[] {
  const result = [];
  for (const e of arr) {
    result.push(e * e);
  }
  return result;
}
