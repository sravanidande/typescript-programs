export function even(x: number): boolean {
  if (x % 2 === 0) {
    return true;
  }
  return false;
}

export function all(f: (x: number) => boolean, arr: number[]): boolean {
  let result = true;
  for (const e of arr) {
    result = result && f(e);
  }
  return result;
}
