import { sum } from './sum';

test('sum', () => {
  expect(sum([2, 3, 4])).toEqual(9);
  expect(sum([6, 4, 5])).toEqual(15);
)};
