export default function allEven(arr: number[]): number[] {
  const result: number[] = [];
  for (const e of arr) {
    if (e % 2 === 0) {
      result.push(e);
    }
  }
  return result;
}
