import { take } from './take';

test('take', () => {
  expect(take(2, [2, 3, 4, 5])).toEqual([2, 3]);
  expect(take(3, [1, 3, 6, 8, 4, 7])).toEqual([1, 3, 6]);
});
