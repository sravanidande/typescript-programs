export default function filter(
  f: (x: number) => boolean,
  arr: number[]
): number[] {
  const result: number[] = [];
  for (const e of arr) {
    if (f(e)) {
      result.push(e);
    }
  }
  return result;
}
