import { drop } from './drop';

test('drop', () => {
  expect(drop(2, [2, 3, 4, 5])).toEqual([4, 5]);
  expect(drop(3, [1, 3, 6, 8, 4, 7])).toEqual([8, 4, 7]);
});
