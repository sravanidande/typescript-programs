export function take(n: number, arr: number[]): number[] {
  const result: number[] = [];
  for (let e = 0; e < n; e++) {
    result.push(arr[e]);
  }
  return result;
}
