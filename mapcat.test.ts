import {concat,concatn,inc,map,mapcat} from './mapcat';
test('concat', () => {
  expect(concat([1,2],[3,4])).toEqual([1,2,3,4]);
});

test('concatn', () => {
  expect(concatn([[1,2,3,4],[5,6]])).toEqual([1,2,3,4,5,6]);
});

test('inc', () => {
  expect(inc(2)).toEqual(3);
});

test('map', () => {
  expect(map(inc,[2,3])).toEqual([3,4]);
});

test('mapcat', () => {
  expect(mapcat(map,[[2,3],[4,5]])).toEqual([2,3,4,5]);
});





