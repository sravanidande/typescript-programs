import {concat} from './mapcat';

export function flatten (arr: number[][]): number[] {
  let result: number[] = [];
  for (const e of arr){
    result= concat(result,e);
  }
  return result;
}