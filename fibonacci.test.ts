import { fibonacci } from './fibonacci';

test('fibonacci' , () => {
  expect (fibonacci(5)).toEqual([1, 1, 2, 3, 5]);
  expect (fibonacci(2)).toEqual([1, 1]);
  expect (fibonacci(4)).toEqual([1, 1, 2, 3]);
  expect (fibonacci(6)).toEqual([1, 1, 2, 3, 5, 8]);
});
